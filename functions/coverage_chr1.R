#### Functions to analyse  and plot the depth on chr1
### Author : R.Guitton MD
### Neuromics research group (Pr Tzoulis)

df_coverage_report_chr1 <- function(){
directory='others/coveragechr1/'
list_of_files <- sort(list.files(path=directory, pattern='*.tsv'))
files_n = length(list_of_files)

df_coverage_chr1=data.frame()
for (i in 1:files_n){
  filename = list_of_files[i]
  samplename = str_extract(filename, pattern="SL[0-9]{6}")
  dataset = read.csv(file = paste0(directory,filename), 
                     sep='\t',
                     as.is=T,
                     header=FALSE,
                     col.names = c('chr','pos','depth')) %>%
    mutate(sample=samplename)
  df_coverage_chr1=rbind(df_coverage_chr1,dataset)
}
return(df_coverage_chr1)
}

plot_coverage_chr1 <- function(df_coverage_chr1){
  
  df_chr1= df_coverage_chr1 %>% filter(pos>624000&pos<632000)
  
  ggplot(data=df_chr1, aes(x = pos,y = depth))+geom_line()+xlab('chromosome 1')+ylab('coverage')+
    facet_grid(rows = vars(sample))
  
}

copy_number_estimation <- function(){
  directory='others/copy_number_estimation/' 
  list_of_files <- sort(list.files(path=directory, pattern='*.txt'))
  files_n = length(list_of_files)
  
  df_copy_number=data.frame()
  for (i in 1:files_n){
    
    filename = list_of_files[i]
    samplename = str_extract(filename, pattern="SL[0-9]{6}")
    dataset = read.csv(file = paste0(directory,filename), 
                       sep='\t',
                       as.is=T,
                       header=FALSE,
                       col.names = c('rname','startpos','endpos',
                                     'numreads','covbases','coverage',
                                     'meandepth','meanbaseq','meanmapq'),skip = 1)
    #uncomment to get the full dataset
    #extract=dataset %>% filter(rname %in% c('chr1','chrM')) %>% mutate(sample=samplename)
    mean_depth_chr1=dataset[dataset$rname=='chr1','meandepth']
    mean_depth_chrM=dataset[dataset$rname=='chrM','meandepth']
    copynumber = (mean_depth_chrM/mean_depth_chr1)*2 #x2 to account for the 2 DNA copies in nDNA
    extract = data.frame(sample=samplename,
                         CN=copynumber,
                         mean_depth_chr1=mean_depth_chr1,
                         mean_depth_chrM=mean_depth_chrM)
    df_copy_number=rbind(df_copy_number,extract)
  }
  return(df_copy_number)
}


