
#### Functions to be verify the absence of non theoritical context (ref = rCRS)
### Author : R.Guitton MD
### Neuromics research group (Pr Tzoulis)



context_finder_mtDNA <-  function(){
mtdna_seq <-read.csv('functions/mtDNA_ref_annotated.txt', sep='\t')
#adding the CpG read in the reverse strand 

### looking for same information from CHH

  
  mtdna_seq$lead <- lead(mtdna_seq$base)
  mtdna_seq$lead_lead <- lead(mtdna_seq$base,n=2)
  mtdna_seq$lag <- lag(mtdna_seq$base)
  mtdna_seq$lag_lag <- lag(mtdna_seq$base,n=2)
  
  mtdna_seq$CpG_direct_strand <- ifelse(mtdna_seq$base=='C',
                                        
                                        ifelse((mtdna_seq$lead=='G'),TRUE,FALSE),FALSE)
  
  
  mtdna_seq$CpG_reverse_strand <- ifelse(mtdna_seq$base=='G',
                                         
                                         ifelse((mtdna_seq$lag=='C'),TRUE,FALSE),FALSE)
  
  mtdna_seq$CpG <- ifelse(mtdna_seq$CpG_direct_strand==TRUE | mtdna_seq$CpG_reverse_strand==TRUE,TRUE,FALSE)
                                         
  
  mtdna_seq$CHH_direct_strand <- ifelse(mtdna_seq$base=='C',
                                        
                                        ifelse((mtdna_seq$lead!='G'&mtdna_seq$lead_lead!='G'),TRUE,FALSE),FALSE)
  
  
  mtdna_seq$CHH_reverse_strand <- ifelse(mtdna_seq$base=='G',
                                         
                                         ifelse((mtdna_seq$lag!='C'&mtdna_seq$lag_lag!='C'),TRUE,FALSE),FALSE)
  
  mtdna_seq$CHH <- ifelse(mtdna_seq$CHH_direct_strand==TRUE | mtdna_seq$CHH_reverse_strand==TRUE,TRUE,FALSE)
  
  
  mtdna_seq$CHG_direct_strand <- ifelse(mtdna_seq$base=='C',
                                        
                                        ifelse((mtdna_seq$lead!='G'&mtdna_seq$lead_lead=='G'),TRUE,FALSE),FALSE)
  
  
  mtdna_seq$CHG_reverse_strand <- ifelse(mtdna_seq$base=='G',
                                         
                                         ifelse((mtdna_seq$lag!='C'&mtdna_seq$lag_lag=='C'),TRUE,FALSE),FALSE)
  
  mtdna_seq$CHG <- ifelse(mtdna_seq$CHG_direct_strand==TRUE | mtdna_seq$CHG_reverse_strand==TRUE,TRUE,FALSE)
  
  mtdna_seq <- mtdna_seq %>% select(-c('lead','lead_lead','lag','lag_lag',
                                       'CHG_direct_strand','CHG_reverse_strand',
                                       'CHH_direct_strand','CHH_reverse_strand',
                                       'CpG_direct_strand','CpG_reverse_strand'))
  
  
  mtdna_seq$theor_context = ifelse(mtdna_seq$CpG==TRUE,'CpG',
                                   ifelse(mtdna_seq$CHH==TRUE,'CHH',
                                          ifelse(mtdna_seq$CHG==TRUE,'CHG',
                                                 NA)))
return(mtdna_seq)
}


