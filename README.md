# mtDNA_methylation

Source code for the paper "Ultra-deep whole genome bisulfite sequencing reveals a single methylation hotspot in human brain mitochondrial DNA" 